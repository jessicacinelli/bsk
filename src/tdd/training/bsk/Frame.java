package tdd.training.bsk;

public class Frame {
	int firstThrow;
	int secondThrow;
	int result;
	boolean spare;
	int bonus=0;
	boolean strike;
	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow)  {
		this.firstThrow=firstThrow;
		this.secondThrow=secondThrow;
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		return firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		// To be implemented
		return secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		this.bonus=bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		
		return bonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		result = firstThrow + secondThrow + bonus;
		return result;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		if(firstThrow==10 || secondThrow ==10)
			strike=true;
		return strike;
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		if(firstThrow + secondThrow ==10)
			spare=true;
		return spare;
	}

	@Override
	public String toString() {
		return "Frame [firstThrow=" + firstThrow + ", secondThrow=" + secondThrow + ", result=" + result + ", spare="
				+ spare + ", bonus=" + bonus + ", strike=" + strike + "]";
	}

}
