package tdd.training.bsk;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class FrameTest {
	private ArrayList<Frame> frames = new ArrayList(10);
	private Game g;
	
	//Tests user story 1
	@Test
	public void test() throws BowlingException{
		Frame f= new Frame(2, 4);
		assertEquals(2, f.getFirstThrow());
		assertEquals(4, f.getSecondThrow());
	}

	//Tests user story 2
		@Test
		public void testFrameScore() throws BowlingException{
			Frame f= new Frame(2, 6);
			assertEquals(8, f.getScore());	
		}
		
	//Tests user story 3
		public void testGame() throws BowlingException{
			this.g = new Game();
			Frame f0= new Frame(1,5);
			g.addFrame(f0);
			frames.add(f0);
			assertEquals(f0, g.getFrameAt(0));
			Frame f1= new Frame(3,6);
			g.addFrame(f1);
			frames.add(f1);
			assertEquals(f1, g.getFrameAt(1));
			Frame f2= new Frame(7,2);
			g.addFrame(f2);
			frames.add(f2);
			assertEquals(f2, g.getFrameAt(2));
			Frame f3= new Frame(3,6);
			g.addFrame(f3);
			frames.add(f3);
			assertEquals(f3, g.getFrameAt(3));
			Frame f4= new Frame(4,4);
			g.addFrame(f4);
			frames.add(f4);
			assertEquals(f4, g.getFrameAt(4));
			Frame f5= new Frame(5,3);
			g.addFrame(f5);
			frames.add(f5);
			assertEquals(f5, g.getFrameAt(5));
			Frame f6= new Frame(3,3);
			g.addFrame(f6);
			frames.add(f6);
			assertEquals(f6, g.getFrameAt(6));
			Frame f7= new Frame(4,5);
			g.addFrame(f7);
			frames.add(f7);
			assertEquals(f7, g.getFrameAt(7));
			Frame f8= new Frame(8,1);
			g.addFrame(f8);
			frames.add(f8);
			assertEquals(f8, g.getFrameAt(8));
			Frame f9= new Frame(2,6);
			g.addFrame(f9);
			frames.add(f9);
			assertEquals(f9, g.getFrameAt(9));
		}
		
		//Tests user story 4
		@Test
		public void testGameScore() throws BowlingException{
			testGame();
			int score =g.calculateScore();
			int scoreExpected=81;
			assertEquals(scoreExpected, score);	
		}
		
	public void testGame2() throws BowlingException{
			this.g = new Game();
			Frame f0= new Frame(1,9);
			g.addFrame(f0);
			frames.add(f0);
			assertEquals(f0, g.getFrameAt(0));
			Frame f1= new Frame(3,6);
			g.addFrame(f1);
			frames.add(f1);
			assertEquals(f1, g.getFrameAt(1));
			Frame f2= new Frame(7,2);
			g.addFrame(f2);
			frames.add(f2);
			assertEquals(f2, g.getFrameAt(2));
			Frame f3= new Frame(3,6);
			g.addFrame(f3);
			frames.add(f3);
			assertEquals(f3, g.getFrameAt(3));
			Frame f4= new Frame(4,4);
			g.addFrame(f4);
			frames.add(f4);
			assertEquals(f4, g.getFrameAt(4));
			Frame f5= new Frame(5,3);
			g.addFrame(f5);
			frames.add(f5);
			assertEquals(f5, g.getFrameAt(5));
			Frame f6= new Frame(3,3);
			g.addFrame(f6);
			frames.add(f6);
			assertEquals(f6, g.getFrameAt(6));
			Frame f7= new Frame(4,5);
			g.addFrame(f7);
			frames.add(f7);
			assertEquals(f7, g.getFrameAt(7));
			Frame f8= new Frame(8,1);
			g.addFrame(f8);
			frames.add(f8);
			assertEquals(f8, g.getFrameAt(8));
			Frame f9= new Frame(2,6);
			g.addFrame(f9);
			frames.add(f9);
			assertEquals(f9, g.getFrameAt(9));
	}
	
		//Tests user story 5
				@Test
				public void testSpare() throws BowlingException{
					testGame2();
					int scoreExpected=88;
					int score =g.calculateScore();
					assertEquals(scoreExpected, score);	
				}
				
				public void testGame3() throws BowlingException{
					this.g = new Game();
					Frame f0= new Frame(10,0);
					g.addFrame(f0);
					frames.add(f0);
					assertEquals(f0, g.getFrameAt(0));
					Frame f1= new Frame(3,6);
					g.addFrame(f1);
					frames.add(f1);
					assertEquals(f1, g.getFrameAt(1));
					Frame f2= new Frame(7,2);
					g.addFrame(f2);
					frames.add(f2);
					assertEquals(f2, g.getFrameAt(2));
					Frame f3= new Frame(3,6);
					g.addFrame(f3);
					frames.add(f3);
					assertEquals(f3, g.getFrameAt(3));
					Frame f4= new Frame(4,4);
					g.addFrame(f4);
					frames.add(f4);
					assertEquals(f4, g.getFrameAt(4));
					Frame f5= new Frame(5,3);
					g.addFrame(f5);
					frames.add(f5);
					assertEquals(f5, g.getFrameAt(5));
					Frame f6= new Frame(3,3);
					g.addFrame(f6);
					frames.add(f6);
					assertEquals(f6, g.getFrameAt(6));
					Frame f7= new Frame(4,5);
					g.addFrame(f7);
					frames.add(f7);
					assertEquals(f7, g.getFrameAt(7));
					Frame f8= new Frame(8,1);
					g.addFrame(f8);
					frames.add(f8);
					assertEquals(f8, g.getFrameAt(8));
					Frame f9= new Frame(2,6);
					g.addFrame(f9);
					frames.add(f9);
					assertEquals(f9, g.getFrameAt(9));
			}	
		
				//Tests user story 6
				@Test
				public void testStrike() throws BowlingException{
					testGame3();
					int scoreExpected=94;
					int score =g.calculateScore();
					assertEquals(scoreExpected, score);	
				}
				/****/
				
				public void testGame4() throws BowlingException{
					this.g = new Game();
					Frame f0= new Frame(10,0);
					g.addFrame(f0);
					frames.add(f0);
					assertEquals(f0, g.getFrameAt(0));
					Frame f1= new Frame(4,6);
					g.addFrame(f1);
					frames.add(f1);
					assertEquals(f1, g.getFrameAt(1));
					Frame f2= new Frame(7,2);
					g.addFrame(f2);
					frames.add(f2);
					assertEquals(f2, g.getFrameAt(2));
					Frame f3= new Frame(3,6);
					g.addFrame(f3);
					frames.add(f3);
					assertEquals(f3, g.getFrameAt(3));
					Frame f4= new Frame(4,4);
					g.addFrame(f4);
					frames.add(f4);
					assertEquals(f4, g.getFrameAt(4));
					Frame f5= new Frame(5,3);
					g.addFrame(f5);
					frames.add(f5);
					assertEquals(f5, g.getFrameAt(5));
					Frame f6= new Frame(3,3);
					g.addFrame(f6);
					frames.add(f6);
					assertEquals(f6, g.getFrameAt(6));
					Frame f7= new Frame(4,5);
					g.addFrame(f7);
					frames.add(f7);
					assertEquals(f7, g.getFrameAt(7));
					Frame f8= new Frame(8,1);
					g.addFrame(f8);
					frames.add(f8);
					assertEquals(f8, g.getFrameAt(8));
					Frame f9= new Frame(2,6);
					g.addFrame(f9);
					frames.add(f9);
					assertEquals(f9, g.getFrameAt(9));
			}	
		
				//Tests user story 7
				@Test
				public void testStrikeSpare() throws BowlingException{
					testGame4();
					int scoreExpected=103;
					int score =g.calculateScore();
					assertEquals(scoreExpected, score);	
				}
				
			//	
				public void testGame5() throws BowlingException{
				this.g = new Game();
				Frame f0= new Frame(10,0);
				g.addFrame(f0);
				frames.add(f0);
				assertEquals(f0, g.getFrameAt(0));
				Frame f1= new Frame(10,0);
				g.addFrame(f1);
				frames.add(f1);
				assertEquals(f1, g.getFrameAt(1));
				Frame f2= new Frame(7,2);
				g.addFrame(f2);
				frames.add(f2);
				assertEquals(f2, g.getFrameAt(2));
				Frame f3= new Frame(3,6);
				g.addFrame(f3);
				frames.add(f3);
				assertEquals(f3, g.getFrameAt(3));
				Frame f4= new Frame(4,4);
				g.addFrame(f4);
				frames.add(f4);
				assertEquals(f4, g.getFrameAt(4));
				Frame f5= new Frame(5,3);
				g.addFrame(f5);
				frames.add(f5);
				assertEquals(f5, g.getFrameAt(5));
				Frame f6= new Frame(3,3);
				g.addFrame(f6);
				frames.add(f6);
				assertEquals(f6, g.getFrameAt(6));
				Frame f7= new Frame(4,5);
				g.addFrame(f7);
				frames.add(f7);
				assertEquals(f7, g.getFrameAt(7));
				Frame f8= new Frame(8,1);
				g.addFrame(f8);
				frames.add(f8);
				assertEquals(f8, g.getFrameAt(8));
				Frame f9= new Frame(2,6);
				g.addFrame(f9);
				frames.add(f9);
				assertEquals(f9, g.getFrameAt(9));
		}	
	
			//Tests user story 8
			@Test
			public void testMultipleStrike() throws BowlingException{
				
				testGame5();
				int scoreExpected=112;
				int score =g.calculateScore();
				assertEquals(scoreExpected, score);	
			}
			public void testGame6() throws BowlingException{
				this.g = new Game();
				Frame f0= new Frame(8,2);
				g.addFrame(f0);
				frames.add(f0);
				assertEquals(f0, g.getFrameAt(0));
				Frame f1= new Frame(5,5);
				g.addFrame(f1);
				frames.add(f1);
				assertEquals(f1, g.getFrameAt(1));
				Frame f2= new Frame(7,2);
				g.addFrame(f2);
				frames.add(f2);
				assertEquals(f2, g.getFrameAt(2));
				Frame f3= new Frame(3,6);
				g.addFrame(f3);
				frames.add(f3);
				assertEquals(f3, g.getFrameAt(3));
				Frame f4= new Frame(4,4);
				g.addFrame(f4);
				frames.add(f4);
				assertEquals(f4, g.getFrameAt(4));
				Frame f5= new Frame(5,3);
				g.addFrame(f5);
				frames.add(f5);
				assertEquals(f5, g.getFrameAt(5));
				Frame f6= new Frame(3,3);
				g.addFrame(f6);
				frames.add(f6);
				assertEquals(f6, g.getFrameAt(6));
				Frame f7= new Frame(4,5);
				g.addFrame(f7);
				frames.add(f7);
				assertEquals(f7, g.getFrameAt(7));
				Frame f8= new Frame(8,1);
				g.addFrame(f8);
				frames.add(f8);
				assertEquals(f8, g.getFrameAt(8));
				Frame f9= new Frame(2,6);
				g.addFrame(f9);
				frames.add(f9);
				assertEquals(f9, g.getFrameAt(9));
		}	
	
			//Tests user story 9
			@Test
			public void testMultipleSpare() throws BowlingException{
				
				testGame6();
				int scoreExpected=98;
				int score =g.calculateScore();
				assertEquals(scoreExpected, score);	
			}
			
			public void testGame7() throws BowlingException{
				this.g = new Game();
				Frame f0= new Frame(1,5);
				g.addFrame(f0);
				frames.add(f0);
				assertEquals(f0, g.getFrameAt(0));
				Frame f1= new Frame(3,6);
				g.addFrame(f1);
				frames.add(f1);
				assertEquals(f1, g.getFrameAt(1));
				Frame f2= new Frame(7,2);
				g.addFrame(f2);
				frames.add(f2);
				assertEquals(f2, g.getFrameAt(2));
				Frame f3= new Frame(3,6);
				g.addFrame(f3);
				frames.add(f3);
				assertEquals(f3, g.getFrameAt(3));
				Frame f4= new Frame(4,4);
				g.addFrame(f4);
				frames.add(f4);
				assertEquals(f4, g.getFrameAt(4));
				Frame f5= new Frame(5,3);
				g.addFrame(f5);
				frames.add(f5);
				assertEquals(f5, g.getFrameAt(5));
				Frame f6= new Frame(3,3);
				g.addFrame(f6);
				frames.add(f6);
				assertEquals(f6, g.getFrameAt(6));
				Frame f7= new Frame(4,5);
				g.addFrame(f7);
				frames.add(f7);
				assertEquals(f7, g.getFrameAt(7));
				Frame f8= new Frame(8,1);
				g.addFrame(f8);
				frames.add(f8);
				assertEquals(f8, g.getFrameAt(8));
				Frame f9= new Frame(2,8);
				g.addFrame(f9);
				frames.add(f9);
				assertEquals(f9, g.getFrameAt(9));
		}	
	
			//Tests user story 10
			@Test
			public void testSpareAtLastFrame() throws BowlingException{
				
				testGame7();
				g.setFirstBonusThrow(7);
				int scoreExpected=90;
				int score =g.calculateScore();
				assertEquals(scoreExpected, score);	
			}
			
		
			public void testGame8() throws BowlingException{
				this.g = new Game();
				Frame f0= new Frame(1,5);
				g.addFrame(f0);
				frames.add(f0);
				assertEquals(f0, g.getFrameAt(0));
				Frame f1= new Frame(3,6);
				g.addFrame(f1);
				frames.add(f1);
				assertEquals(f1, g.getFrameAt(1));
				Frame f2= new Frame(7,2);
				g.addFrame(f2);
				frames.add(f2);
				assertEquals(f2, g.getFrameAt(2));
				Frame f3= new Frame(3,6);
				g.addFrame(f3);
				frames.add(f3);
				assertEquals(f3, g.getFrameAt(3));
				Frame f4= new Frame(4,4);
				g.addFrame(f4);
				frames.add(f4);
				assertEquals(f4, g.getFrameAt(4));
				Frame f5= new Frame(5,3);
				g.addFrame(f5);
				frames.add(f5);
				assertEquals(f5, g.getFrameAt(5));
				Frame f6= new Frame(3,3);
				g.addFrame(f6);
				frames.add(f6);
				assertEquals(f6, g.getFrameAt(6));
				Frame f7= new Frame(4,5);
				g.addFrame(f7);
				frames.add(f7);
				assertEquals(f7, g.getFrameAt(7));
				Frame f8= new Frame(8,1);
				g.addFrame(f8);
				frames.add(f8);
				assertEquals(f8, g.getFrameAt(8));
				Frame f9= new Frame(10,0);
				g.addFrame(f9);
				frames.add(f9);
				assertEquals(f9, g.getFrameAt(9));
		}	
	
			//Tests user story 11
			@Test
			public void testStrikeAtLastFrame() throws BowlingException{
				
				testGame8();
				g.setFirstBonusThrow(7);
				g.setSecondBonusThrow(2);
				int scoreExpected=92;
				int score =g.calculateScore();
				assertEquals(scoreExpected, score);	
			}
			
			public void testGame10() throws BowlingException{
				this.g = new Game();
				Frame f0= new Frame(10,0);
				g.addFrame(f0);
				frames.add(f0);
				assertEquals(f0, g.getFrameAt(0));
				Frame f1= new Frame(10,0);
				g.addFrame(f1);
				frames.add(f1);
				assertEquals(f1, g.getFrameAt(1));
				Frame f2= new Frame(10,0);
				g.addFrame(f2);
				frames.add(f2);
				assertEquals(f2, g.getFrameAt(2));
				Frame f3= new Frame(10,0);
				g.addFrame(f3);
				frames.add(f3);
				assertEquals(f3, g.getFrameAt(3));
				Frame f4= new Frame(10,0);
				g.addFrame(f4);
				frames.add(f4);
				assertEquals(f4, g.getFrameAt(4));
				Frame f5= new Frame(10,0);
				g.addFrame(f5);
				frames.add(f5);
				assertEquals(f5, g.getFrameAt(5));
				Frame f6= new Frame(10,0);
				g.addFrame(f6);
				frames.add(f6);
				assertEquals(f6, g.getFrameAt(6));
				Frame f7= new Frame(10,0);
				g.addFrame(f7);
				frames.add(f7);
				assertEquals(f7, g.getFrameAt(7));
				Frame f8= new Frame(10,0);
				g.addFrame(f8);
				frames.add(f8);
				assertEquals(f8, g.getFrameAt(8));
				Frame f9= new Frame(10,0);
				g.addFrame(f9);
				frames.add(f9);
				assertEquals(f9, g.getFrameAt(9));
		}	
	
			//Tests user story 12
			@Test
			public void testBestScore() throws BowlingException{
				
				testGame10();
				g.setFirstBonusThrow(10);
				g.setSecondBonusThrow(10);
				int scoreExpected=300;
				int score =g.calculateScore();
				assertEquals(scoreExpected, score);	
			}
		
	}
