package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	private ArrayList<Frame> frames;
	private int firstBonusThrow;
	private int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new ArrayList<>(10);
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame)  {
		frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) {

		return  frames.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow)  {
		this.firstBonusThrow= firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow)  {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {

		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {

		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore()  {
		int totalScore=0;
		for(int i=0; i<10; i++) {
			if(getFrameAt(i).isStrike()) {
				if(i==9)
					getFrameAt(i).setBonus( getFirstBonusThrow() + getSecondBonusThrow());
				else if(getFrameAt(i+1).isStrike() && ((i+1)!=9)) 
					getFrameAt(i).setBonus(getFrameAt(i+1).getScore() + getFrameAt(i+2).getFirstThrow());
				else if ((i+1)==9)

					getFrameAt(i).setBonus(getFrameAt(i+1).getScore() + getFirstBonusThrow());


				else if (getFrameAt(i+1).isSpare())
					getFrameAt(i).setBonus(getFrameAt(i+1).getScore());
				else
					getFrameAt(i).setBonus(getFrameAt(i+1).getScore());
			}
			else if(getFrameAt(i).isSpare()) {
				if(i==9)
					getFrameAt(i).setBonus( getFirstBonusThrow());

				else if( (i+1)!=9)
					getFrameAt(i).setBonus(getFrameAt(i+1).getFirstThrow());


			}
			totalScore=totalScore+getFrameAt(i).getScore();
		}
		
		return totalScore;	
	}

}
